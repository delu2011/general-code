# Manage nginx webserver
class nginx {
  package { 'apache2.2-common':
    ensure => absent,
  }

  package { 'nginx':
    ensure => installed,
    require => Package['apache2.2-common'],
  }

  service { 'nginx':
    ensure  => running,
    enable => true,
    require => Package['nginx'],
    restart => '/usr/sbin/service ssh reload',
  }

  $whisper_dirs = [ '/var/www', '/var/www/cat-pictures',
                  ]

  file { $whisper_dirs:
    ensure => 'directory',
  }

  file { '/var/www/cat-pictures/index.html':
    source => 'puppet:///modules/nginx/index.html',
    notify => Service['nginx'],
    require => File['/var/www/cat-pictures']
  }

  file { '/etc/nginx/sites-enabled/cat-pictures':
    source => 'puppet:///modules/nginx/cat-pictures.conf',
    notify => Service['nginx'],
  }

}
