# import libraries
import urllib2
from bs4 import BeautifulSoup

# specify the url
quote_page = 'http://dashing-dashboards.service/summary'

# query the website and return the html to the variable 'page'
page = urllib2.urlopen(quote_page)

# parse the html using beautiful soap and store in variable `soup`
soup = BeautifulSoup(page, 'html.parser')

# Take out the <div> of name and get its value
for n in soup.find_all('div'):
    print "data-title:", n.get('data-title')
